package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.SiteOffice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SiteOfficeRepo extends JpaRepository<SiteOffice, Long> {
    List<SiteOffice> findByProjectID(long projectID);
}
