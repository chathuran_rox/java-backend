package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.IssueModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IssueRepository extends JpaRepository<IssueModel,Long> {
    List<IssueModel> findByType(String s);
}