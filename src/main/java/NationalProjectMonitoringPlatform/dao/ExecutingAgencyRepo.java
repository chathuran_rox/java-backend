package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.ExecutingAgency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ExecutingAgencyRepo extends JpaRepository <ExecutingAgency,Long>
{
	@Query(value = "SELECT * FROM npms.executing_agency;",nativeQuery = true)
	List<ExecutingAgency> getAllExeAgencies();
}

