package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.ImplementingAgency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImplementingAgencyRepo extends JpaRepository<ImplementingAgency,Long>
{
}
