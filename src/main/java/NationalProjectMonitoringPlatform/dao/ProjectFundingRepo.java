package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.ProjectFunding;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectFundingRepo extends JpaRepository<ProjectFunding,Long>
{
}
