package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.Institutes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstitutesRepository extends JpaRepository<Institutes, Long> {
}
