package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.Issue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IssuesRepo extends JpaRepository<Issue,Long>
{

	@Query(value = "SELECT * FROM npms.issue where project_id =?;",nativeQuery = true)
	List<Issue> getIssues(Long projectID);
}
