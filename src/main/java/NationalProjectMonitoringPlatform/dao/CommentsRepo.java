package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentsRepo extends JpaRepository<Comments,Long>
{
}
