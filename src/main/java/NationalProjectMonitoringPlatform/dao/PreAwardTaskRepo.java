package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.PreAwardTask;
import NationalProjectMonitoringPlatform.modle.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PreAwardTaskRepo extends JpaRepository<PreAwardTask,Long>
{
	@Query(value = "SELECT * FROM npms.np_preawardtask where parent_task =?",nativeQuery = true)
	List<PreAwardTask> getChildren(Long parentId);

}
