package NationalProjectMonitoringPlatform.dao;


import NationalProjectMonitoringPlatform.modle.Program;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProgramRepo extends JpaRepository<Program,Long>
{
	@Query(value = "select title from npms.program",nativeQuery = true)
	List<String> getProgramTitle();

	@Query(value = "select * from npms.program where id =?",nativeQuery = true)
	Program getFirst( Long id );

}
