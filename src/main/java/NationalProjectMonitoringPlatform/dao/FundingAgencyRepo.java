package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.FundingAgency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FundingAgencyRepo extends JpaRepository<FundingAgency,Long>
{
}
