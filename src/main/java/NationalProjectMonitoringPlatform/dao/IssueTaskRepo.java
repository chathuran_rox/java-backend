package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.Issue;
import NationalProjectMonitoringPlatform.modle.IssueTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IssueTaskRepo extends JpaRepository<IssueTask,Long>
{
	@Query(value = "SELECT * FROM npms.issue_task where issue_id =?;",nativeQuery = true)
	List<IssueTask> getIssueTasks(Long id);
}
