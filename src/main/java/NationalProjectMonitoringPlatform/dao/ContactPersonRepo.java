package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.ContactPerson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactPersonRepo extends JpaRepository<ContactPerson,Long>
{
}
