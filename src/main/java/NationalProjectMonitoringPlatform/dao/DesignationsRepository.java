package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.Designations;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesignationsRepository extends JpaRepository<Designations, Long> {
}
