package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaskRepo extends JpaRepository<Task,Long>
{
	@Query(value = "SELECT * FROM npms.task where parent_task_id =?",nativeQuery = true)
	List<Task> getChildren(Long parentId);

//	@Query("SELECT new map ( i.id as task_id, i.taskTitle as title, i.projects as projects) FROM Task i order by i.taskTitle")
//	List<Task> getAllTasks();
}
