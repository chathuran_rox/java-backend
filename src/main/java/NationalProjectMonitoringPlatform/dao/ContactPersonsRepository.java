package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.ContactPersons;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactPersonsRepository extends JpaRepository<ContactPersons, Long> {
}
