package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.IssueAssign;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IssueAssignRepository extends JpaRepository<IssueAssign, Long> {
}
