package NationalProjectMonitoringPlatform.dao;

import NationalProjectMonitoringPlatform.modle.PreAwardTask;
import NationalProjectMonitoringPlatform.modle.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by chathuran on 1/5/2018.
 */

public interface ProjectRepository extends JpaRepository<Project,Long>
{
	@Query(value = "SELECT * FROM npms.np_project where project_id=?",nativeQuery = true)
	Project getProject(Long projectid);

	@Query("SELECT new map ( i.projectId as projectId, i.projectName as projectName, i.agreementDate as agreementDate, i.description as description, i.vision as vision, i.mission as mission, i.duration as duration) FROM Project i order by i.projectName")
	List<Project> getAllProjectsForProgressMonitoring();

	@Query("SELECT new map ( i.projectId as projectId, i.projectName as projectName, i.agreementDate as agreementDate, i.description as description, i.vision as vision, i.mission as mission, i.duration as duration) FROM Project i where i.projectName like ?1%")
	Page<Project> findByProjectNameStartingWith(String projectName, Pageable pageable);

	Project findByProjectId(Long id);
}
