package NationalProjectMonitoringPlatform.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.File;

@Component
public class EmailServiceImpl implements EmailService {

    @Autowired
    public JavaMailSender emailSender;

    public void sendSimpleMessage(@Email @NotNull @NotBlank String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);

            emailSender.send(message);
        } catch (MailException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void sendSimpleMessageUsingTemplate(@Email @NotNull @NotBlank String to,
                                               String subject,
                                               SimpleMailMessage template,
                                               String ...templateArgs) {
        String text = String.format(template.getText(), templateArgs);
        sendSimpleMessage(to, subject, text);
    }

    @Override
    public void sendMessageWithAttachment(@Email @NotNull @NotBlank String to,
                                          String subject,
                                          String text,
                                          String pathToAttachment) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            // pass 'true' to the constructor to create a multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);

            FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
            helper.addAttachment("Invoice", file);

            emailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void htmlemail() {
        try{
            MimeMessage message = emailSender.createMimeMessage();
            boolean multipart = true;
            String name= "Chathuran Dannoruwa";
            MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");
            String htmlMsg = "<h3>Welcome "+ name+"</h3>"
                    +"<img src='http://192.168.1.170:8080/images/logo.jpg'>";
            message.setContent(htmlMsg, "text/html");
            helper.setTo("npmp.lka@gmail.com");
            helper.setSubject("Test send HTML email");
            this.emailSender.send(message);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
    }

    public void RegisterUser(String name, String password, String Email) {
        try{
            MimeMessage message = emailSender.createMimeMessage();
            boolean multipart = true;
            MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");
            String htmlMsg = "<img src='http://192.168.1.170:8080/images/logo.jpg'>"
                    +"<h4>"+name+",<br/>You are welcome to the <b>National Management Platform.</b></h4>"+"<h4>Your Usename is : <b>" +Email+ "</b></h4>"+"<h4>Your Password is : <b>" +password+ "</b></h4>";
            message.setContent(htmlMsg, "text/html");
            helper.setTo("npmp.lka@gmail.com");
            helper.setSubject("Welcome to National Project Management Platform");
            this.emailSender.send(message);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
    }

    public void IssueCreated(String name, String password, String Email) {
        try{
            MimeMessage message = emailSender.createMimeMessage();
            boolean multipart = true;
            MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");
            String htmlMsg = "<img src='http://192.168.1.170:8080/images/logo.jpg'>"
                    +"<h4>"+name+",<br/>You are welcome to the <b>National Management Platform.</b></h4>"+"<h4>Your Usename is : <b>" +Email+ "</b></h4>"+"<h4>Your Password is : <b>" +password+ "</b></h4>";
            message.setContent(htmlMsg, "text/html");
            helper.setTo("npmp.lka@gmail.com");
            helper.setSubject("Welcome to National Project Management Platform");
            this.emailSender.send(message);
        }catch (Exception ex){
            System.out.println(ex.toString());
        }
    }
}
