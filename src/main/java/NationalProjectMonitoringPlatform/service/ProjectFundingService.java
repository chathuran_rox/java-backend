package NationalProjectMonitoringPlatform.service;

import NationalProjectMonitoringPlatform.dao.ContactPersonRepo;
import NationalProjectMonitoringPlatform.dao.FundingAgencyRepo;
import NationalProjectMonitoringPlatform.dao.ProjectFundingRepo;
import NationalProjectMonitoringPlatform.modle.FundingAgency;
import NationalProjectMonitoringPlatform.modle.ProjectFunding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectFundingService
{
	@Autowired
	private ProjectFundingRepo projectFundingRepo;

	@Autowired
	private ContactPersonRepo contactPersonRepo;

	@Autowired
	private FundingAgencyRepo fundingAgencyRepo;

	public List<ProjectFunding> savefundings( List<ProjectFunding> fundings )
	{
		for ( ProjectFunding funding : fundings )
		{
			funding.setContactPerson( contactPersonRepo.save( funding.getContactPerson() ) );
			funding.setFundingAgency( fundingAgencyRepo.save( funding.getFundingAgency() ) );
		}
		return projectFundingRepo.saveAll( fundings );
	}

	public List<ProjectFunding> getAllProjectFundings()
	{
		return projectFundingRepo.findAll();
	}
}
