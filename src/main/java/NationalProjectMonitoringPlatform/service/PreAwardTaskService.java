package NationalProjectMonitoringPlatform.service;

import NationalProjectMonitoringPlatform.dao.ContactPersonRepo;
import NationalProjectMonitoringPlatform.dao.PreAwardTaskRepo;
import NationalProjectMonitoringPlatform.modle.ContactPerson;
import NationalProjectMonitoringPlatform.modle.PreAwardTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class PreAwardTaskService
{
	@Autowired
	private PreAwardTaskRepo preAwardTaskRepo;

	@Autowired
	private ContactPersonRepo contactPersonRepo;

	public List<PreAwardTask> getAllTasks(){
		List<PreAwardTask> preaward = preAwardTaskRepo.findAll();
//
//		for(PreAwardTask pat:preaward){
//			pat.getContactPersonPreAward().setProject( null );
//		}
		return preaward;
	}

	public PreAwardTask savePreAwardTask(PreAwardTask task)
	{
//		task.setContactPerson( contactPersonRepo.save( task.getContactPerson() ) );
		ContactPerson cp =contactPersonRepo.save( task.getContactPersonPreAward());
		task.setContactPersonPreAward( cp );
		return preAwardTaskRepo.save( task );
	}

	public List<PreAwardTask> savePreawardTaskSet(List<PreAwardTask> tasks)
	{

//		List<PreAwardTask> returnList = new ArrayList<PreAwardTask>(  );
//
//		for(PreAwardTask pt: tasks){
//			ContactPerson cp = contactPersonRepo.save( pt.getContactPersonPreAward() );
//			pt.setContactPersonPreAward( cp );
//			returnList.add( preAwardTaskRepo.save( pt ) ) ;
//		}
//
//		return returnList;
		return preAwardTaskRepo.saveAll( tasks );
	}

	public List<PreAwardTask> getChildTasks(Long id)
	{
		return preAwardTaskRepo.getChildren( id );
	}


}
