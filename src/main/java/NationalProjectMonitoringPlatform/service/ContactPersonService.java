package NationalProjectMonitoringPlatform.service;

import NationalProjectMonitoringPlatform.dao.ContactPersonRepo;
import NationalProjectMonitoringPlatform.modle.ContactPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactPersonService
{
	@Autowired
	private ContactPersonRepo contactPersonRepo;

	public List<ContactPerson> getAll()
	{
		return contactPersonRepo.findAll();
	}
}
