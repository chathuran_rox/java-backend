package NationalProjectMonitoringPlatform.service;


import NationalProjectMonitoringPlatform.dao.ProgramRepo;
import NationalProjectMonitoringPlatform.modle.Program;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgramService
{
	@Autowired
	private ProgramRepo programRepo;

	public Program saveProgram(Program program)
	{
		return programRepo.save( program );
	}

	public List<String> getProgramNames()
	{
		return programRepo.getProgramTitle();
	}

	public Program getFirstProgram(Long id)
	{
		return programRepo.getFirst( id );
	}

	public List<Program> getAllPrograms()
	{
		return programRepo.findAll();
	}
}
