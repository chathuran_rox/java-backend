package NationalProjectMonitoringPlatform.service;


import NationalProjectMonitoringPlatform.dao.CommentsRepo;
import NationalProjectMonitoringPlatform.modle.Comments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentsService
{
	@Autowired
	private CommentsRepo commentsRepo;


	public Comments saveComments(Comments comments)
	{

		return commentsRepo.save( comments );
	}

	public List<Comments> getAll()
	{

		return commentsRepo.findAll();
	}
}
