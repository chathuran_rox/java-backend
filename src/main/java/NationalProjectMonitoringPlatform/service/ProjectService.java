package NationalProjectMonitoringPlatform.service;

import NationalProjectMonitoringPlatform.dao.ContactPersonRepo;
import NationalProjectMonitoringPlatform.dao.ProjectRepository;
import NationalProjectMonitoringPlatform.modle.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chathuran on 8/20/2018.
 */
@Service
public class ProjectService
{
	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private ContactPersonRepo contactPersonRepo;

	@Autowired
	private AgencyService agencyService;

	@Autowired
	private ProjectFundingService projectFundingService;

	@Autowired
	private PreAwardTaskService preAwardTaskService;


	public List<Project> getAllProject(){
		return projectRepository.findAll();
	}

	public Project saveProject(Project project)
	{

		if(project.getContactPerson() != null){
			project.setContactPerson( contactPersonRepo.save(project.getContactPerson()));
		}

//		project.setProjectFunding(projectFundingService.savefundings( project.getProjectFunding() )  );

		if(project.getExecutingAgencies() != null){
			project.setExecutingAgencies( agencyService.saveExecutingAgency( project.getExecutingAgencies() ) );
		}

		if(project.getImplementingAgencies() != null){
			project.setImplementingAgencies( agencyService.saveImpAgency( project.getImplementingAgencies() ) );
		}

		if(project.getPreAwardTaskSet() != null){
			project.setPreAwardTaskSet( preAwardTaskService.savePreawardTaskSet(project.getPreAwardTaskSet()));
		}

		Project savedProject = projectRepository.save( project );

		if(savedProject.getProjectFunding() != null){
		List<ProjectFunding> pf = savedProject.getProjectFunding();
		for(ProjectFunding projF : pf){
			projF.setProject( savedProject );
		}
		projectFundingService.savefundings( pf );}
		return savedProject;
	}

	public Project getProject(Long id)
	{
		return projectRepository.getProject( id );
	}

	public Project getSingleProject(Long id){
		return  projectRepository.findByProjectId(id);
	}

	public List<PreAwardTask> getAllPreAwards(Long projectId)
	{
		List<PreAwardTask> preAwards =new ArrayList<PreAwardTask>(  );
		List<PreAwardTask> parentPreAwards = new ArrayList<PreAwardTask>(  );

		try
		{
			preAwards = projectRepository.getProject( projectId ).getPreAwardTaskSet();
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}

		if(preAwards != null){
			for(PreAwardTask task:preAwards)
			{
				if(task.getParentTaskId() == 0 )
				{
					task.setProjects( null );
					parentPreAwards.add( task );
				}
			}

		}

			return parentPreAwards;





	}

	public List<Task> getAllTaskBreakdown(Long projectid)
	{
		List<Task> taskbreakdown = new ArrayList<Task>(  );
		List<Task> parentTaskBreakdown = new ArrayList<Task>(  );

		try
		{
			taskbreakdown = projectRepository.getProject( projectid ).getTasks();
		}

		catch ( Exception e )
		{
			e.printStackTrace();
		}


		if(taskbreakdown != null){
			for(Task task:taskbreakdown)
			{
				if(task.getParentTaskId() ==0)
				{
					task.setProjects( null );
					parentTaskBreakdown.add( task );
				}
			}
		}
		return parentTaskBreakdown;
	}

	public List<Project> getAllProjectsForProgressMonitoring(){
		return projectRepository.getAllProjectsForProgressMonitoring();
	}

	public Page<Project> findByProjectNameStartingWith(String projectName, Pageable pageable){
		return projectRepository.findByProjectNameStartingWith(projectName, pageable);
	}
}
