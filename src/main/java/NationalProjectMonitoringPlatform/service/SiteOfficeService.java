package NationalProjectMonitoringPlatform.service;

import NationalProjectMonitoringPlatform.dao.SiteOfficeRepo;
import NationalProjectMonitoringPlatform.modle.SiteOffice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SiteOfficeService {

    @Autowired
    private SiteOfficeRepo siteOfficeRepo;

    public SiteOffice saveSiteOffice(SiteOffice siteOffice, Long parent){
        if(parent!=0){
            siteOffice.setParent(siteOfficeRepo.getOne(parent));
        }
        else{
            siteOffice.setParent(null);
        }
        return siteOfficeRepo.save( siteOffice );
    }

    public List<SiteOffice> getAll(){
        return siteOfficeRepo.findAll();
    }

    public SiteOffice getSiteOffice(Long id){ return siteOfficeRepo.findById(id).orElse(null);}

    public void deleteSiteOffice(SiteOffice siteOffice){ siteOfficeRepo.delete(siteOffice); }

    public void updateSiteOfficeRepo(Long id){ siteOfficeRepo.flush();}
}
