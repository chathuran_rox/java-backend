package NationalProjectMonitoringPlatform.service;

import NationalProjectMonitoringPlatform.dao.ContactPersonRepo;
import NationalProjectMonitoringPlatform.dao.TaskRepo;
import NationalProjectMonitoringPlatform.modle.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService
{
	@Autowired
	private TaskRepo taskRepo;

	@Autowired
	private ContactPersonRepo contactPersonRepo;

	public Task saveTask( Task task )
	{

		task.setContactPerson( contactPersonRepo.save( task.getContactPerson() ) );
		return taskRepo.save( task );
	}

	public List<Task> getTask()
	{
		return taskRepo.findAll();
	}

	public Task updateLeaf( Long id )
	{
		Task task = taskRepo.getOne( id );
		task.setLeaf( false );
		return taskRepo.save( task );
	}

	public List<Task> getChildren( Long parentid )
	{

		return taskRepo.getChildren( parentid );
	}

	public Task updateTaskProgressAndStatus( Task task)
		{
			Task tempTask = taskRepo.getOne( task.getId() );
			tempTask.setStatus( task.getStatus() );
			tempTask.setCompletedProgress( task.getCompletedProgress() );
			return taskRepo.save( tempTask );
		}

}
