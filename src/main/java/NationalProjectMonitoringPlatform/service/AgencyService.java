package NationalProjectMonitoringPlatform.service;

import NationalProjectMonitoringPlatform.dao.ExecutingAgencyRepo;
import NationalProjectMonitoringPlatform.dao.FundingAgencyRepo;
import NationalProjectMonitoringPlatform.dao.ImplementingAgencyRepo;
import NationalProjectMonitoringPlatform.modle.ExecutingAgency;
import NationalProjectMonitoringPlatform.modle.FundingAgency;
import NationalProjectMonitoringPlatform.modle.ImplementingAgency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class AgencyService
{
	@Autowired
	private ExecutingAgencyRepo executingAgencyRepo;

	@Autowired
	private ImplementingAgencyRepo implementingAgencyRepo;

	@Autowired
	private FundingAgencyRepo fundingAgencyRepo;

	public List<ExecutingAgency> saveExecutingAgency( List<ExecutingAgency> exeAgency )
	{

		return executingAgencyRepo.saveAll( exeAgency );
	}

	public List<ExecutingAgency> getAllExeAgencies()
	{

		List<ExecutingAgency> exeAgencies = executingAgencyRepo.findAll();
		for ( ExecutingAgency ex : exeAgencies )
		{
			ex.setProjects( null );
		}

		return exeAgencies;
	}

	public List<ImplementingAgency> saveImpAgency( List<ImplementingAgency> implementingAgency )
	{
		return implementingAgencyRepo.saveAll( implementingAgency );
	}

	public List<ImplementingAgency> getAllImpAgencies()
	{

		List<ImplementingAgency> implAgencies = implementingAgencyRepo.findAll();

		for ( ImplementingAgency imp : implAgencies )
		{
			imp.setProjects( null );
		}

		return implAgencies;
	}

	public List<FundingAgency> getAllFundingAgencies()
	{
		return  fundingAgencyRepo.findAll();

	}
}
