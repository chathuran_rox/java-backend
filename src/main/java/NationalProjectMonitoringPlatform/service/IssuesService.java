package NationalProjectMonitoringPlatform.service;

import NationalProjectMonitoringPlatform.dao.ContactPersonRepo;
import NationalProjectMonitoringPlatform.dao.IssueTaskRepo;
import NationalProjectMonitoringPlatform.dao.IssuesRepo;
import NationalProjectMonitoringPlatform.modle.ContactPerson;
import NationalProjectMonitoringPlatform.modle.Issue;
import NationalProjectMonitoringPlatform.modle.IssueTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IssuesService
{
	@Autowired
	private IssuesRepo issuesRepo;

	@Autowired
	private IssueTaskRepo issueTaskRepo;

	@Autowired
	private ContactPersonRepo contactPersonRepo;

	public List<Issue> getAllIssues(Long id)
	{
		return issuesRepo.getIssues( id );
	}

	public  List<IssueTask> getAllIssueTasks(Long id)
	{
		return issueTaskRepo.getIssueTasks( id );
	}

	public Issue saveIssue(Issue issue)
	{
		issue.setContactPerson( contactPersonRepo.save( issue.getContactPerson() ) );
		return issuesRepo.save( issue );
	}

	public IssueTask saveIssueTask(IssueTask issueTask)
	{
		issueTask.setContactPerson( contactPersonRepo.save( issueTask.getContactPerson() ) );
		return issueTaskRepo.save( issueTask );
	}
}
