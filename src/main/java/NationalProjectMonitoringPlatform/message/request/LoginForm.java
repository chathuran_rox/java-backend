package NationalProjectMonitoringPlatform.message.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Getter
@Setter
public class LoginForm {
    @NotNull
    @NotBlank
    @Email
    private String username;

    @NotBlank
    @NotNull
    private String password;
}
