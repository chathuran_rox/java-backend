package NationalProjectMonitoringPlatform.message.request;

import NationalProjectMonitoringPlatform.security.validPassword.ValidPassword;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;


@Getter
@Setter
public class SignUpForm {
    @NotNull
    @NotBlank
    private String name;

    @NotNull
    @NotBlank
    @Size(max = 60)
    @Email
    private String username;

    @NotNull
    @NotBlank
    @Size(max = 60)
    @Email
    private String email;

    @NotNull
    private Set<String> role;
}
