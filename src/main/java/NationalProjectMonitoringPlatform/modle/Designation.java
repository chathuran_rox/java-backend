package NationalProjectMonitoringPlatform.modle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by chathuran on 8/21/2018.
 */
@Entity
@Table(name = "NP_DESIGNATION")
public class Designation
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DES_ID")
	private long desId;
	@Column(name = "DESIGNATION")
	private String designation;

	public Designation()
	{
	}

	public Designation( long desId, String designation )
	{
		this.desId = desId;
		this.designation = designation;
	}

	public long getDesId()
	{
		return desId;
	}

	public void setDesId( long desId )
	{
		this.desId = desId;
	}

	public String getDesignation()
	{
		return designation;
	}

	public void setDesignation( String designation )
	{
		this.designation = designation;
	}
}
