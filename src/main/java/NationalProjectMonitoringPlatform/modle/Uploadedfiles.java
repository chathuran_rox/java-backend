package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@SuppressWarnings("Lombok")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "uploadedfiles")
public class Uploadedfiles {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "upload_file_id")
    private long uploadFileID;

    @ManyToOne
    @JoinColumn(name = "issue_id")
    @JsonBackReference
    private IssueModel issue;

    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
    private String type;

    public Uploadedfiles(String fileName, String fileDownloadUri, String fileType, long size, String type) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
        this.type = type;
    }
}