package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import javax.persistence.*;
import java.util.List;

@Entity
public class Issue
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long issueId;

	private String issueTitle;
	private String issueDescription;
	private String issueAttachement;

	@ManyToOne
	@JoinColumn(name = "project_id")
	@JsonIgnoreProperties("issues")
	private Project projectIssue;

	@ManyToOne
	@JoinColumn(name = "projectTask_id")
	@JsonIgnoreProperties("issues")
	private Task taskIssue;

	@ManyToOne
	@JoinColumn(name = "contactperson_id")
	@JsonIgnoreProperties("issues")
	private ContactPerson contactPerson;

	@OneToMany(mappedBy = "issue",cascade = CascadeType.ALL)
	private List<IssueTask> issueTasks;

	public Issue( Long issueId, String issueTitle, String issueDescription, String issueAttachement, Project projectIssue, Task taskIssue, ContactPerson contactPerson, List<IssueTask> issueTasks )
	{
		this.issueId = issueId;
		this.issueTitle = issueTitle;
		this.issueDescription = issueDescription;
		this.issueAttachement = issueAttachement;
		this.projectIssue = projectIssue;
		this.taskIssue = taskIssue;
		this.contactPerson = contactPerson;
		this.issueTasks = issueTasks;
	}

	public Issue()
	{
	}

	public Long getIssueId()
	{
		return issueId;
	}

	public void setIssueId( Long issueId )
	{
		this.issueId = issueId;
	}

	public String getIssueTitle()
	{
		return issueTitle;
	}

	public void setIssueTitle( String issueTitle )
	{
		this.issueTitle = issueTitle;
	}

	public String getIssueDescription()
	{
		return issueDescription;
	}

	public void setIssueDescription( String issueDescription )
	{
		this.issueDescription = issueDescription;
	}

	public String getIssueAttachement()
	{
		return issueAttachement;
	}

	public void setIssueAttachement( String issueAttachement )
	{
		this.issueAttachement = issueAttachement;
	}

	public Project getProjectIssue()
	{
		return projectIssue;
	}

	public void setProjectIssue( Project projectIssue )
	{
		this.projectIssue = projectIssue;
	}

	public Task getTaskIssue()
	{
		return taskIssue;
	}

	public void setTaskIssue( Task taskIssue )
	{
		this.taskIssue = taskIssue;
	}

	public ContactPerson getContactPerson()
	{
		return contactPerson;
	}

	public void setContactPerson( ContactPerson contactPerson )
	{
		this.contactPerson = contactPerson;
	}

	public List<IssueTask> getIssueTasks()
	{
		return issueTasks;
	}

	public void setIssueTasks( List<IssueTask> issueTasks )
	{
		this.issueTasks = issueTasks;
	}


}
