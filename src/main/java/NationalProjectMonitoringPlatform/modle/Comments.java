package NationalProjectMonitoringPlatform.modle;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Comments
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String comment;
	private Date commentDate;
	private String commentatorName;
	private String commentatorPosition;

	public Comments()
	{
	}

	public Comments( Long id, String comment, Date commentDate, String commentatorName, String commentatorPosition )
	{
		this.id = id;
		this.comment = comment;
		this.commentDate = commentDate;
		this.commentatorName = commentatorName;
		this.commentatorPosition = commentatorPosition;
	}

	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	public Date getCommentDate()
	{
		return commentDate;
	}

	public void setCommentDate( Date commentDate )
	{
		this.commentDate = commentDate;
	}

	public String getCommentatorName()
	{
		return commentatorName;
	}

	public void setCommentatorName( String commentatorName )
	{
		this.commentatorName = commentatorName;
	}

	public String getCommentatorPosition()
	{
		return commentatorPosition;
	}

	public void setCommentatorPosition( String commentatorPosition )
	{
		this.commentatorPosition = commentatorPosition;
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment( String comment )
	{
		this.comment = comment;
	}
}
