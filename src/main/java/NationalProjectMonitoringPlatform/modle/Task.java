package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class Task
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String taskTitle;
	private String status;
	private String weight;
	private String description;
	private Date startDate;
	private String agreedAmount;

	private Integer unit;
	private Date endDate;
	private String monitoringAgency;

	private Long parentTaskId;
	private boolean isLeaf;


	private Date createDate;
	private Date completedDate;
	private Long completedProgress;
	private String attachements;
	private String skypeID;

	private String currencyType;
	private String unitType;


//	@ManyToMany(mappedBy = "tasks")
//	@JsonIgnoreProperties("tasks")
//	private List<Project> projects;

	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(
			name = "project_taskbreakdown",
			joinColumns = {@JoinColumn(name="task_id",referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name="project_id",referencedColumnName = "PROJECT_ID")}
	)
	@JsonIgnoreProperties("tasks")
	private Set<Project> projects;


	@OneToMany(mappedBy = "taskIssue",cascade = CascadeType.ALL)
	private List<Issue> issues;

	@ManyToOne
	@JoinColumn(name = "contactperson_id")
	@JsonIgnoreProperties("task") //break the infinite recursion cycle
	private ContactPerson contactPerson;

	public Task( Long id, String taskTitle, String status, String weight, String description, Date startDate, String agreedAmount, Integer unit, Date endDate, String monitoringAgency, Long parentTaskId, boolean isLeaf, Date createDate, Date completedDate, Long completedProgress, String attachements, String skypeID, String currencyType, String unitType, Set<Project> projects, List<Issue> issues, ContactPerson contactPerson )
	{
		this.id = id;
		this.taskTitle = taskTitle;
		this.status = status;
		this.weight = weight;
		this.description = description;
		this.startDate = startDate;
		this.agreedAmount = agreedAmount;
		this.unit = unit;
		this.endDate = endDate;
		this.monitoringAgency = monitoringAgency;
		this.parentTaskId = parentTaskId;
		this.isLeaf = isLeaf;
		this.createDate = createDate;
		this.completedDate = completedDate;
		this.completedProgress = completedProgress;
		this.attachements = attachements;
		this.skypeID = skypeID;
		this.currencyType = currencyType;
		this.unitType = unitType;
		this.projects = projects;
		this.issues = issues;
		this.contactPerson = contactPerson;
	}

	public Task()
	{
	}

	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	public String getTaskTitle()
	{
		return taskTitle;
	}

	public void setTaskTitle( String taskTitle )
	{
		this.taskTitle = taskTitle;
	}

	public Long getParentTaskId()
	{
		return parentTaskId;
	}

	public void setParentTaskId( Long parentTaskId )
	{
		this.parentTaskId = parentTaskId;
	}

	public boolean isLeaf()
	{
		return isLeaf;
	}

	public void setLeaf( boolean leaf )
	{
		isLeaf = leaf;
	}

	public String getMonitoringAgency()
	{
		return monitoringAgency;
	}

	public void setMonitoringAgency( String monitoringAgency )
	{
		this.monitoringAgency = monitoringAgency;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus( String status )
	{
		this.status = status;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate( Date startDate )
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate( Date endDate )
	{
		this.endDate = endDate;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription( String description )
	{
		this.description = description;
	}

	public String getAgreedAmount()
	{
		return agreedAmount;
	}

	public void setAgreedAmount( String agreedAmount )
	{
		this.agreedAmount = agreedAmount;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate( Date createDate )
	{
		this.createDate = createDate;
	}

	public Date getCompletedDate()
	{
		return completedDate;
	}

	public void setCompletedDate( Date completedDate )
	{
		this.completedDate = completedDate;
	}

	public Long getCompletedProgress()
	{
		return completedProgress;
	}

	public void setCompletedProgress( Long completedProgress )
	{
		this.completedProgress = completedProgress;
	}

	public String getAttachements()
	{
		return attachements;
	}

	public void setAttachements( String attachements )
	{
		this.attachements = attachements;
	}

	public Integer getUnit()
	{
		return unit;
	}

	public void setUnit( Integer unit )
	{
		this.unit = unit;
	}

	public String getWeight()
	{
		return weight;
	}

	public void setWeight( String weight )
	{
		this.weight = weight;
	}

	public Set<Project> getProjects()
	{
		return projects;
	}

	public void setProjects( Set<Project> projects )
	{
		this.projects = projects;
	}

	public List<Issue> getIssues()
	{
		return issues;
	}

	public void setIssues( List<Issue> issues )
	{
		this.issues = issues;
	}

	public String getSkypeID()
	{
		return skypeID;
	}

	public void setSkypeID( String skypeID )
	{
		this.skypeID = skypeID;
	}

	public ContactPerson getContactPerson()
	{
		return contactPerson;
	}

	public void setContactPerson( ContactPerson contactPerson )
	{
		this.contactPerson = contactPerson;
	}

	public String getCurrencyType()
	{
		return currencyType;
	}

	public void setCurrencyType( String currencyType )
	{
		this.currencyType = currencyType;
	}

	public String getUnitType()
	{
		return unitType;
	}

	public void setUnitType( String unitType )
	{
		this.unitType = unitType;
	}
}
