package NationalProjectMonitoringPlatform.modle;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@Entity
public class Program
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String title;
	private Date startDate;
	private Date endDate;
	private String email;
	private String phone;
	private String description;
	private String responsPerson;
	private String createdBy;
	private String modifiedBy;
	private Date modifiedDate;

	public Program()
	{
	}

	public Program( Long id, String title, Date startDate, Date endDate, String email, String phone, String description, String responsPerson, String createdBy, String modifiedBy, Date modifiedDate )
	{
		this.id = id;
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
		this.email = email;
		this.phone = phone;
		this.description = description;
		this.responsPerson = responsPerson;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
	}

	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle( String title )
	{
		this.title = title;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate( Date startDate )
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate( Date endDate )
	{
		this.endDate = endDate;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail( String email )
	{
		this.email = email;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone( String phone )
	{
		this.phone = phone;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription( String description )
	{
		this.description = description;
	}

	public String getResponsPerson()
	{
		return responsPerson;
	}

	public void setResponsPerson( String responsPerson )
	{
		this.responsPerson = responsPerson;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy( String createdBy )
	{
		this.createdBy = createdBy;
	}

	public String getModifiedBy()
	{
		return modifiedBy;
	}

	public void setModifiedBy( String modifiedBy )
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate()
	{
		return modifiedDate;
	}

	public void setModifiedDate( Date modifiedDate )
	{
		this.modifiedDate = modifiedDate;
	}
}
