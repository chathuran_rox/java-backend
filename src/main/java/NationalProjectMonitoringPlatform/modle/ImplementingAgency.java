package NationalProjectMonitoringPlatform.modle;

import javax.persistence.*;
import javax.xml.ws.Service;
import java.util.Set;

@Entity
public class ImplementingAgency
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ImpelmentingAgencyId;

	private String IAName;

	@ManyToMany(mappedBy = "implementingAgencies")
	private Set<Project> projects;

	public ImplementingAgency()
	{
	}

	public ImplementingAgency( Long impelmentingAgencyId, String IAName, Set<Project> projects )
	{
		this.ImpelmentingAgencyId = impelmentingAgencyId;
		this.IAName = IAName;
		this.projects = projects;
	}

	public Long getImpelmentingAgencyId()
	{
		return ImpelmentingAgencyId;
	}

	public void setImpelmentingAgencyId( Long impelmentingAgencyId )
	{
		ImpelmentingAgencyId = impelmentingAgencyId;
	}

	public String getIAName()
	{
		return IAName;
	}

	public void setIAName( String IAName )
	{
		this.IAName = IAName;
	}

	public Set<Project> getProjects()
	{
		return projects;
	}

	public void setProjects( Set<Project> projects )
	{
		this.projects = projects;
	}
}
