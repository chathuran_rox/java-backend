package NationalProjectMonitoringPlatform.modle;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "IssueAssign")
public class IssueAssign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "issue_assign_id")
    @JsonProperty("issue_assign_id")
    @ApiModelProperty(notes = "The database generated issue assign ID")
    private long issueAssignId;

    @NotBlank(message = "ProjectTaskID can not be a blank")
    @NotNull(message = "ProjectTaskID can not be null")
    @Column(name = "ProjectTaskID")
    @JsonProperty("ProjectTaskID")
    private long ProjectTaskID;

    @NotBlank(message = "IssueTypeID can not be a blank")
    @NotNull(message = "IssueTypeID can not be null")
    @Column(name = "IssueTypeID")
    @JsonProperty("IssueTypeID")
    private long IssueTypeID;

    @NotBlank(message = "IssueSeverityID can not be a blank")
    @NotNull(message = "IssueSeverityID can not be null")
    @Column(name = "IssueSeverityID")
    @JsonProperty("IssueSeverityID")
    private long IssueSeverityID;

    @NotBlank(message = "ResponsiblePersonID can not be a blank")
    @NotNull(message = "ResponsiblePersonID can not be null")
    @Column(name = "ResponsiblePersonID")
    @JsonProperty("ResponsiblePersonID")
    private long ResponsiblePersonID;


    @Column(name = "StartDate")
    @JsonProperty("StartDate")
    private Date StartDate;

    @Column(name = "EndDate")
    @JsonProperty("EndDate")
    private Date EndDate;

    @NotBlank(message = "IssueID can not be a blank")
    @NotNull(message = "IssueID can not be null")
    @Column(name = "IssueID")
    @JsonProperty("IssueID")
    private long IssueID;

    @NotBlank(message = "ProjectID can not be a blank")
    @NotNull(message = "ProjectID can not be null")
    @Column(name = "ProjectID")
    @JsonProperty("ProjectID")
    private long ProjectID;

}
