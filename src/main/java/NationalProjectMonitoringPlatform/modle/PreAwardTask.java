package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by chathuran on 8/21/2018.
 */
@Entity
@Table(name = "NP_PREAWARDTASK")
public class PreAwardTask
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long id;
	@Column(name = "TASK_TITLE")
	private String taskTitle;
	@Column(name = "PARENT_TASK")
	private long parentTaskId;
	@Column(name = "RESPONSIBLE_PERSON")
	private long responsiblePerson;
	@Column(name = "STATUS")
	private long status;
	@Column(name = "START_DATE", nullable = true)
	@ColumnDefault( "'2018-01-01'" )
	private Date startDate;
	@Column(name = "END_DATE", nullable = true)
	@ColumnDefault( "'2018-01-01'" )
	private Date endDate;
	@Column(name = "DESCRIPTION")
	private String description;

	private String preAwardAttachements;

	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(
			name = "NP_PROJECT_PREAWARDTASK",
			joinColumns = {@JoinColumn(name="PRE_AWARDTASK_ID")},
			inverseJoinColumns = {@JoinColumn(name="PROJECT_ID")}
	)
	@JsonIgnoreProperties("preAwardTaskSet")
	private Set<Project> projects;


//	@ManyToMany(mappedBy = "preAwardTaskSet")
//	@JsonIgnoreProperties("preAwardTaskSet")
//	private List<Project> projects;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "contactPersonPreAward_id")
	@JsonIgnoreProperties("preAwardTasks")
	private ContactPerson contactPersonPreAward;


	public PreAwardTask()
	{
	}

//	public PreAwardTask( long id, String taskTitle, long parentTaskId, long responsiblePerson, long status, Date startDate )
//	{
//		this.id = id;
//		this.taskTitle = taskTitle;
//		this.parentTaskId = parentTaskId;
//		this.responsiblePerson = responsiblePerson;
//		this.status = status;
//		this.startDate = startDate;
//	}

	public PreAwardTask( long id, String taskTitle, long parentTaskId, long responsiblePerson, long status, Date startDate, Date endDate, String description, String preAwardAttachements, Set<Project> projects, ContactPerson contactPersonPreAward )
	{
		this.id = id;
		this.taskTitle = taskTitle;
		this.parentTaskId = parentTaskId;
		this.responsiblePerson = responsiblePerson;
		this.status = status;
		this.startDate = startDate;
		this.endDate = endDate;
		this.description = description;
		this.preAwardAttachements = preAwardAttachements;
		this.projects = projects;
		this.contactPersonPreAward = contactPersonPreAward;
	}

	public long getId()
	{
		return id;
	}

	public void setId( long id )
	{
		this.id = id;
	}

	public String getTaskTitle()
	{
		return taskTitle;
	}

	public void setTaskTitle( String taskTitle )
	{
		this.taskTitle = taskTitle;
	}

	public long getParentTaskId()
	{
		return parentTaskId;
	}

	public void setParentTaskId( long parentTaskId )
	{
		this.parentTaskId = parentTaskId;
	}

	public long getResponsiblePerson()
	{
		return responsiblePerson;
	}

	public void setResponsiblePerson( long responsiblePerson )
	{
		this.responsiblePerson = responsiblePerson;
	}

	public long getStatus()
	{
		return status;
	}

	public void setStatus( long status )
	{
		this.status = status;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate( Date startDate )
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate( Date endDate )
	{
		this.endDate = endDate;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription( String description )
	{
		this.description = description;
	}

	public ContactPerson getContactPersonPreAward()
	{
		return contactPersonPreAward;
	}

	public void setContactPersonPreAward( ContactPerson contactPersonPreAward )
	{
		this.contactPersonPreAward = contactPersonPreAward;
	}

	public String getPreAwardAttachements()
	{
		return preAwardAttachements;
	}

	public void setPreAwardAttachements( String preAwardAttachements )
	{
		this.preAwardAttachements = preAwardAttachements;
	}

	public Set<Project> getProjects()
	{
		return projects;
	}

	public void setProjects( Set<Project> projects )
	{
		this.projects = projects;
	}
}
