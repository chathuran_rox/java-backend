package NationalProjectMonitoringPlatform.modle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by chathuran on 8/21/2018.
 */
@Entity
@Table(name = "NP_ORGANIZATIONSTRUCTURE")
public class OrganizationStructure
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ORG_ID")
	private long orgId;
	@Column(name = "ORG_TITLE")
	private String orgTitle;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CONTACT_ID")
//	@Column(name = "KEY_CONTACT_PERSON_ID")
	private ContactPerson keyContactPerson;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROJECT_ID")
	private Project project;

	public OrganizationStructure()
	{
	}

	public OrganizationStructure( long orgId, String orgTitle,
			ContactPerson keyContactPerson, Project project )
	{
		this.orgId = orgId;
		this.orgTitle = orgTitle;
		this.keyContactPerson = keyContactPerson;
		this.project = project;
	}

	public long getOrgId()
	{
		return orgId;
	}

	public void setOrgId( long orgId )
	{
		this.orgId = orgId;
	}

	public String getOrgTitle()
	{
		return orgTitle;
	}

	public void setOrgTitle( String orgTitle )
	{
		this.orgTitle = orgTitle;
	}

	public ContactPerson getKeyContactPerson()
	{
		return keyContactPerson;
	}

	public void setKeyContactPerson( ContactPerson keyContactPerson )
	{
		this.keyContactPerson = keyContactPerson;
	}

	public Project getProject()
	{
		return project;
	}

	public void setProject( Project project )
	{
		this.project = project;
	}
}
