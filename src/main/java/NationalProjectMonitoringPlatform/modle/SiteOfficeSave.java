package NationalProjectMonitoringPlatform.modle;


import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
public class SiteOfficeSave {
    private SiteOffice siteOffice;
    private long parentid;
    private long siteOfficeId;
    private List<Parent> siteOfficeParents = new ArrayList<>( 0 );

    public SiteOfficeSave(SiteOffice siteOffice, long parentid) {
        this.siteOffice = siteOffice;
        this.parentid = parentid;
        fetchSiteOfficeParents(siteOffice);
    }

    public void fetchSiteOfficeParents(SiteOffice siteOffice){
        while (siteOffice.getParent()!=null){
            siteOfficeParents.add(new Parent(siteOffice.getParent().getTitle(), siteOffice.getParent().getSiteOfficeId()));
            siteOffice = siteOffice.getParent();
        }
        siteOfficeParents.add(new Parent("Main Organization", 0l));
    }

    public long getParentid() {
        return parentid;
    }
}

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class Parent{
    private String title;
    private Long parentId;
}
