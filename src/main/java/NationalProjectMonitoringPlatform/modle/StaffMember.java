package NationalProjectMonitoringPlatform.modle;


import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "StaffMember")
public class StaffMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "staff_member_id")
    @ApiModelProperty(notes = "The database generated Staff Member ID")
    private long staffMemberId;

    @Column(name = "position")
    private String position;

    @NotNull(message = "Site Office Staff Member Person Name can not be null")
    @NotBlank(message = "Site Office Staff Member Person Name can not be a blank")
    @Column(name = "person")
    private String person;

//    @Column(name = "appointed")
//    private boolean appointed;

    @Column(name = "skypeID")
    private String skypeId;

    @Column(name = "start_date")
    @ColumnDefault( "'2018-01-01'" )
    private Date startDate;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "site_office_id")
    @JsonBackReference
    private SiteOffice staffMemberSiteOffice;
}
