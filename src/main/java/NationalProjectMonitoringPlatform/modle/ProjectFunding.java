package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ProjectFunding
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long projectFundingId;
	private String fundingType;
	private Long amount;
	private int pecentage;
	private float interestRate;
	private Date repaymentPeriod;
	private Date gracePeriod;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "project_id")
	@JsonIgnoreProperties("projectFunding")
	private Project project;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fundingAgency_id")
	@JsonIgnoreProperties("projectFunding")
	private FundingAgency fundingAgency;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "contactPerson_id")
	@JsonIgnoreProperties("projectFundings")
	private ContactPerson contactPerson;

	public ProjectFunding( Long projectFundingId, String fundingType, Long amount, int pecentage, float interestRate, Date repaymentPeriod, Date gracePeriod, Project project, FundingAgency fundingAgency, ContactPerson contactPerson )
	{
		this.projectFundingId = projectFundingId;
		this.fundingType = fundingType;
		this.amount = amount;
		this.pecentage = pecentage;
		this.interestRate = interestRate;
		this.repaymentPeriod = repaymentPeriod;
		this.gracePeriod = gracePeriod;
		this.project = project;
		this.fundingAgency = fundingAgency;
		this.contactPerson = contactPerson;
	}

	public ProjectFunding()
	{
	}

	public Long getProjectFundingId()
	{
		return projectFundingId;
	}

	public void setProjectFundingId( Long projectFundingId )
	{
		this.projectFundingId = projectFundingId;
	}

	public String getFundingType()
	{
		return fundingType;
	}

	public void setFundingType( String fundingType )
	{
		this.fundingType = fundingType;
	}

	public Long getAmount()
	{
		return amount;
	}

	public void setAmount( Long amount )
	{
		this.amount = amount;
	}

	public int getPecentage()
	{
		return pecentage;
	}

	public void setPecentage( int pecentage )
	{
		this.pecentage = pecentage;
	}

	public float getInterestRate()
	{
		return interestRate;
	}

	public void setInterestRate( float interestRate )
	{
		this.interestRate = interestRate;
	}

	public Date getRepaymentPeriod()
	{
		return repaymentPeriod;
	}

	public void setRepaymentPeriod( Date repaymentPeriod )
	{
		this.repaymentPeriod = repaymentPeriod;
	}

	public Date getGracePeriod()
	{
		return gracePeriod;
	}

	public void setGracePeriod( Date gracePeriod )
	{
		this.gracePeriod = gracePeriod;
	}

//	public Project getProject()
//	{
//		return project;
//	}

	public void setProject( Project project )
	{
		this.project = project;
	}

	public FundingAgency getFundingAgency()
	{
		return fundingAgency;
	}

	public void setFundingAgency( FundingAgency fundingAgency )
	{
		this.fundingAgency = fundingAgency;
	}

	public ContactPerson getContactPerson()
	{
		return contactPerson;
	}

	public void setContactPerson( ContactPerson contactPerson )
	{
		this.contactPerson = contactPerson;
	}

	public Project getProject()
	{
		return project;
	}
}
