package NationalProjectMonitoringPlatform.modle;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "Institutes")
public class Institutes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "institute_id")
    @JsonProperty("institute_id")
    @ApiModelProperty(notes = "The database generated institute ID")
    private long instituteID;

    private String institute;
}
