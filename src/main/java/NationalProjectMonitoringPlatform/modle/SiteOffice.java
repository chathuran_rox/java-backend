package NationalProjectMonitoringPlatform.modle;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "SiteOffice")
public class SiteOffice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "site_office_id")
    @JsonProperty("site_office_id")
    @ApiModelProperty(notes = "The database generated Site Office ID")
    private long siteOfficeId;

    @NotBlank(message = "Site Office Title can not be a blank")
    @NotNull(message = "Site Office Title can not be null")
    @Column(name = "title")
    private String title;

    @Column(name = "projectID")
    private long projectID;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "responsible_person")
    private String responsiblePerson;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "email")
    private String email;


    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "staffMemberSiteOffice", orphanRemoval=true)
    @JsonManagedReference
    private List<StaffMember> staffMembers = new ArrayList<>( 0 );

    @ManyToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "parent_id")
    @JsonIgnore
    private SiteOffice parent;

    @OneToMany(mappedBy = "parent")
    @JsonIgnore
    private Set<SiteOffice> subordinates = new HashSet<>();

    @Transient
    @JsonIgnore
    private boolean isLeaf = true;
}
