package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
public class IssueTask
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long issueTaskID;

	private String issueTitle;
	private String issueType;
	private String issueSeverity;
	private Date startDate;
	private Date endDate;
	private String status;
	private Long parentIssueTaskID;

	@ManyToOne
	@JoinColumn(name = "issue_id")
	@JsonIgnoreProperties("issueTasks")
	private Issue issue;

	@ManyToOne
	@JoinColumn(name = "contactperson_id")
	@JsonIgnoreProperties("issueTasks")
	private ContactPerson contactPerson;

	public IssueTask( Long issueTaskID, String issueTitle, String issueType, String issueSeverity, Date startDate, Date endDate, String status, Long parentIssueTaskID, Issue issue, ContactPerson contactPerson )
	{
		this.issueTaskID = issueTaskID;
		this.issueTitle = issueTitle;
		this.issueType = issueType;
		this.issueSeverity = issueSeverity;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
		this.parentIssueTaskID = parentIssueTaskID;
		this.issue = issue;
		this.contactPerson = contactPerson;
	}

	public IssueTask()
	{
	}

	public Long getIssueTaskID()
	{
		return issueTaskID;
	}

	public void setIssueTaskID( Long issueTaskID )
	{
		this.issueTaskID = issueTaskID;
	}

	public String getIssueTitle()
	{
		return issueTitle;
	}

	public void setIssueTitle( String issueTitle )
	{
		this.issueTitle = issueTitle;
	}

	public String getIssueType()
	{
		return issueType;
	}

	public void setIssueType( String issueType )
	{
		this.issueType = issueType;
	}

	public String getIssueSeverity()
	{
		return issueSeverity;
	}

	public void setIssueSeverity( String issueSeverity )
	{
		this.issueSeverity = issueSeverity;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate( Date startDate )
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate( Date endDate )
	{
		this.endDate = endDate;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus( String status )
	{
		this.status = status;
	}

	public Long getParentIssueTaskID()
	{
		return parentIssueTaskID;
	}

	public void setParentIssueTaskID( Long parentIssueTaskID )
	{
		this.parentIssueTaskID = parentIssueTaskID;
	}

	public Issue getIssue()
	{
		return issue;
	}

	public void setIssue( Issue issue )
	{
		this.issue = issue;
	}

	public ContactPerson getContactPerson()
	{
		return contactPerson;
	}

	public void setContactPerson( ContactPerson contactPerson )
	{
		this.contactPerson = contactPerson;
	}
}
