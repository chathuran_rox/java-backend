package NationalProjectMonitoringPlatform.modle;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "ContactPersons")
public class ContactPersons {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contact_person_id")
    @JsonProperty("contact_person_id")
    @ApiModelProperty(notes = "The database generated Contact Person ID")
    private long contactPersonId;

    private String userName;

    private String contactNumber;

    private String skypeID;

    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "designation_id")
    private Designations designation;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "institute_id")
    private Institutes institute;
}
