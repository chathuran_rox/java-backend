package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class FundingAgency
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long agencyId;

	private String agencyName;
	private String agencyAddress;
	private String agencyCountry;

	@OneToMany(mappedBy = "fundingAgency")
	@JsonIgnore
	private List<ProjectFunding> projectFunding;

	public FundingAgency( Long agencyId, String agencyName, String agencyAddress, String agencyCountry )
	{
		this.agencyId = agencyId;
		this.agencyName = agencyName;
		this.agencyAddress = agencyAddress;
		this.agencyCountry = agencyCountry;
	}

	public FundingAgency()
	{
	}

	public Long getAgencyId()
	{
		return agencyId;
	}

	public void setAgencyId( Long agencyId )
	{
		this.agencyId = agencyId;
	}

	public String getAgencyName()
	{
		return agencyName;
	}

	public void setAgencyName( String agencyName )
	{
		this.agencyName = agencyName;
	}

	public String getAgencyAddress()
	{
		return agencyAddress;
	}

	public void setAgencyAddress( String agencyAddress )
	{
		this.agencyAddress = agencyAddress;
	}

	public String getAgencyCountry()
	{
		return agencyCountry;
	}

	public void setAgencyCountry( String agencyCountry )
	{
		this.agencyCountry = agencyCountry;
	}

	public List<ProjectFunding> getProjectFunding()
	{
		return projectFunding;
	}

	public void setProjectFunding( List<ProjectFunding> projectFunding )
	{
		this.projectFunding = projectFunding;
	}
}
