package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;

@Entity
public class ExecutingAgency
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long ExecutingAgencyId;
	private String EAName;


	@ManyToMany(mappedBy = "executingAgencies")
	@JsonIgnoreProperties("executingAgencies")
	private Set<Project> projects;

	public ExecutingAgency()
	{
	}

	public ExecutingAgency( Long executingAgencyId, String EAName )
	{
		ExecutingAgencyId = executingAgencyId;
		this.EAName = EAName;
	}

	public ExecutingAgency( Long executingAgencyId, String EAName, Set<Project> projects )
	{
		ExecutingAgencyId = executingAgencyId;
		this.EAName = EAName;
		this.projects = projects;
	}

	public Long getExecutingAgencyId()
	{
		return ExecutingAgencyId;
	}

	public void setExecutingAgencyId( Long executingAgencyId )
	{
		ExecutingAgencyId = executingAgencyId;
	}

	public String getEAName()
	{
		return EAName;
	}

	public void setEAName( String EAName )
	{
		this.EAName = EAName;
	}

	public Set<Project> getProjects()
	{
		return projects;
	}

	public void setProjects( Set<Project> projects )
	{
		this.projects = projects;
	}
}
