package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "IssueModel")
public class IssueModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long issueID;

    private int projectID;

    private int districtId;

    private  int taskID;

    private String issueTitle;

    private String reportingPerson;

    private String contactNumber;

    private String email;

    private String description;

    private String type;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "issue", orphanRemoval=true)
    @JsonManagedReference
    private List<Uploadedfiles> uploadedFiles = new ArrayList<>( 0 );

    public IssueModel(String issueTitle, List<Uploadedfiles> uploadedFiles) {
        this.issueTitle = issueTitle;
        this.uploadedFiles = uploadedFiles;
    }
}
