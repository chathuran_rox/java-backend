package NationalProjectMonitoringPlatform.modle;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "Designations")
public class Designations {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "designation_id")
    @JsonProperty("designation_id")
    @ApiModelProperty(notes = "The database generated Designation ID")
    private long designationId;

    private String designation;
}
