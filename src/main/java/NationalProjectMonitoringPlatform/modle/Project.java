package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by chathuran on 1/5/2018.
 */
@Entity
@Table(name = "NP_PROJECT")
public class Project
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PROJECT_ID", nullable = true)
	private long projectId;
	@Column(name = "PROJECT_NAME", nullable = true)
	private String projectName;
	@Column(name = "PROJECT_Owner", nullable = true)
	private String projectOwner;
	@Column(name = "STATUS_ID", nullable = true)
	private long statusId;
	@Column(name = "START_DATE", nullable = true)
	@ColumnDefault( "'2018-01-01'" )
	private Date startDate;
	@Column(name = "END_DATE", nullable = true)
	@ColumnDefault( "'2018-01-01'" )
	private Date endDate;
	@Column(name = "DURATION", nullable = true)
	private Long duration;
	@Column(name = "KEY_CONTACT_PERSON_ID", nullable = true)
	private Long keyContactPersonId;
	@Column(name = "CONTRACTER")
	private Long contracter;
	@Column(name = "AGREEMENT_DATE", nullable = true)
	@ColumnDefault( "'2018-01-01'" )
	private Date agreementDate;

	@Column(name = "VISION")
	private String vision;
	@Column(name = "MISSION")
	private String  mission;
	@Column(name = "DESCRIPTION")
	private String description;

	@ManyToMany(mappedBy = "projects")
	@JsonIgnoreProperties("projects")
	private List<PreAwardTask> preAwardTaskSet;


	@ManyToMany(mappedBy = "projects")
	@JsonIgnoreProperties("projects")
	private List<Task> tasks;

//	@ManyToMany(cascade = CascadeType.MERGE)
//	@JoinTable(
//			name = "NP_PROJECT_PREAWARDTASK",
//			joinColumns = {@JoinColumn(name="PROJECT_ID")},
//			inverseJoinColumns = {@JoinColumn(name="PRE_AWARDTASK_ID")}
//	)
//	@JsonIgnoreProperties("projects")
//	private List<PreAwardTask> preAwardTaskSet;

	@ManyToOne
	@JoinColumn(name = "contactperson_id")
	@JsonIgnoreProperties("project") //break the infinite recursion cycle
	private ContactPerson contactPerson;


	@OneToMany(mappedBy = "project",fetch = FetchType.EAGER,orphanRemoval = true)
	@JsonIgnoreProperties("project")
	private List<ProjectFunding> projectFunding;

	@ManyToMany
	@JoinTable(name = "project_implementingAgency", joinColumns = @JoinColumn(name = "project_id", referencedColumnName = "PROJECT_ID"), inverseJoinColumns = @JoinColumn(name = "implementingAgency_id", referencedColumnName = "ImpelmentingAgencyId"))
	@JsonIgnoreProperties("projects")
	private List<ImplementingAgency> implementingAgencies;

	@ManyToMany
	@JoinTable(name = "project_executingAgency", joinColumns = @JoinColumn(name = "project_id",referencedColumnName = "PROJECT_ID"), inverseJoinColumns = @JoinColumn(name = "executingAgency_id",referencedColumnName = "ExecutingAgencyId"))
	@JsonIgnoreProperties("projects")
	private List<ExecutingAgency> executingAgencies;

//	@ManyToMany(mappedBy = "projects")
//	@JsonIgnoreProperties("projects")
//	private List<ExecutingAgency> executingAgencies;

//	@ManyToMany
//	@JoinTable(name = "project_taskbreakdown", joinColumns = @JoinColumn(name = "project_id",referencedColumnName = "PROJECT_ID"),inverseJoinColumns = @JoinColumn(name = "task_id",referencedColumnName = "id"))
//	private List<Task> tasks;


	@OneToMany(mappedBy = "projectIssue",cascade = CascadeType.ALL)
	private List<Issue> issues;

	//	@Column(name = "CLASSIFICATION_ID", nullable = false)
	//	private Long classificationId;
	//	@Column(name = "DISTRICT_ID", nullable = false)
	//	private Long districtId;
	//	@Column(name = "ELECTOTAL_ID", nullable = false)
	//	private Long electoralId;



//	@Column(name = "MINISTRY_ID", nullable = false)
//	private Long ministryId;
//	@Column(name = "OWNER_ID", nullable = false)
//	private Long ownerId;
//	@Column(name = "PROVINCE_ID", nullable = false)
//	private Long provinceId;
//	@Column(name = "LOAN_AGRREMENT_ID", nullable = false)
//	private Long loanAgreementId;
//	@Column(name = "PROJ_AGREMENT_ID", nullable = false)
//	private Long projAgreementId;
//	@Column(name = "SECTOR_ID", nullable = false)
//	private Long sectorId;

//	@Column(name = "ERR", nullable = false)
//	private Long err;
//	@Column(name = "FRR", nullable = false)
//	private Long frr;

//	@Column(name = "DEVELOPMENT_INDICATORS", nullable = false)
//	private Long developmentIndicators;
//	@Column(name = "BUDGET_REF_NO", nullable = false)
//	private String budgetRefNo;

	public Project()
	{
	}

	public Project( long projectId, String projectName, String projectOwner, long statusId, Date startDate,
			Date endDate, Long duration, Long keyContactPersonId, Long contracter, Date agreementDate,
			String vision, String mission, String description,
			List<PreAwardTask> preAwardTaskSet, List<Task> tasks,
			ContactPerson contactPerson,
			List<ProjectFunding> projectFunding,
			List<ImplementingAgency> implementingAgencies,
			List<ExecutingAgency> executingAgencies, List<Issue> issues )
	{
		this.projectId = projectId;
		this.projectName = projectName;
		this.projectOwner = projectOwner;
		this.statusId = statusId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.duration = duration;
		this.keyContactPersonId = keyContactPersonId;
		this.contracter = contracter;
		this.agreementDate = agreementDate;
		this.vision = vision;
		this.mission = mission;
		this.description = description;
		this.preAwardTaskSet = preAwardTaskSet;
		this.contactPerson = contactPerson;
		this.projectFunding = projectFunding;
		this.implementingAgencies = implementingAgencies;
		this.executingAgencies = executingAgencies;
		this.tasks = tasks;
		this.issues = issues;
	}

	public Long getProjectId()
	{
		return projectId;
	}

	public void setProjectId( Long projectId )
	{
		this.projectId = projectId;
	}

	public String getProjectName()
	{
		return projectName;
	}

	public void setProjectName( String projectName )
	{
		this.projectName = projectName;
	}

	public String getProjectOwner()
	{
		return projectOwner;
	}

	public void setProjectOwner( String projectOwner )
	{
		this.projectOwner = projectOwner;
	}

	public long getStatusId()
	{
		return statusId;
	}

	public void setStatusId( Long statusId )
	{
		this.statusId = statusId;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate( Date startDate )
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate( Date endDate )
	{
		this.endDate = endDate;
	}

	public Long getDuration()
	{
		return duration;
	}

	public void setDuration( Long duration )
	{
		this.duration = duration;
	}

	public Long getKeyContactPersonId()
	{
		return keyContactPersonId;
	}

	public void setKeyContactPersonId( Long keyContactPersonId )
	{
		this.keyContactPersonId = keyContactPersonId;
	}

	public Long getContracter()
	{
		return contracter;
	}

	public void setContracter( Long contracter )
	{
		this.contracter = contracter;
	}

	public Date getAgreementDate()
	{
		return agreementDate;
	}

	public void setAgreementDate( Date agreementDate )
	{
		this.agreementDate = agreementDate;
	}

	public String getVision()
	{
		return vision;
	}

	public void setVision( String vision )
	{
		this.vision = vision;
	}

	public String getMission()
	{
		return mission;
	}

	public void setMission( String mission )
	{
		this.mission = mission;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription( String description )
	{
		this.description = description;
	}

	public ContactPerson getContactPerson()
	{
		return contactPerson;
	}

	public void setContactPerson( ContactPerson contactPerson )
	{
		this.contactPerson = contactPerson;
	}

	public List<PreAwardTask> getPreAwardTaskSet()
	{
		return preAwardTaskSet;
	}

	public void setPreAwardTaskSet( List<PreAwardTask> preAwardTaskSet )
	{
		this.preAwardTaskSet = preAwardTaskSet;
	}

	public void setProjectId( long projectId )
	{
		this.projectId = projectId;
	}

	public void setStatusId( long statusId )
	{
		this.statusId = statusId;
	}

	public List<ProjectFunding> getProjectFunding()
	{
		return projectFunding;
	}

	public void setProjectFunding( List<ProjectFunding> projectFunding )
	{
		this.projectFunding = projectFunding;
	}

	public List<ImplementingAgency> getImplementingAgencies()
	{
		return implementingAgencies;
	}

	public void setImplementingAgencies( List<ImplementingAgency> implementingAgencies )
	{
		this.implementingAgencies = implementingAgencies;
	}

	public List<ExecutingAgency> getExecutingAgencies()
	{
		return executingAgencies;
	}

	public void setExecutingAgencies( List<ExecutingAgency> executingAgencies )
	{
		this.executingAgencies = executingAgencies;
	}

	public List<Task> getTasks()
	{
		return tasks;
	}

	public void setTasks( List<Task> tasks )
	{
		this.tasks = tasks;
	}

	public List<Issue> getIssues()
	{
		return issues;
	}

	public void setIssues( List<Issue> issues )
	{
		this.issues = issues;
	}


}

