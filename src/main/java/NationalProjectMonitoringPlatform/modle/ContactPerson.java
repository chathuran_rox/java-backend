package NationalProjectMonitoringPlatform.modle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by chathuran on 8/21/2018.
 */
@Entity
@Table(name = "NP_CONTACTPERSON")
public class ContactPerson
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CONTACT_ID")
	private long contactId;
	private String title;
	private String firstName;
	private String LastName;
	private String email;
	private String contactNo;


	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DES_ID")
	private Designation designation;

	private String skypeID;


	@OneToMany(mappedBy = "contactPerson")
	@JsonIgnoreProperties("contactPerson")
	private List<Project> project;

	@OneToMany(mappedBy = "contactPerson")
	@JsonIgnoreProperties("contactPerson")
	private List<ProjectFunding> projectFundings;

	@OneToMany(mappedBy = "contactPersonPreAward")
	@JsonIgnoreProperties("contactPersonPreAward")
	private List<PreAwardTask> preAwardTasks;

	@OneToMany(mappedBy = "contactPerson")
	@JsonIgnoreProperties("contactPerson")
	private List<Issue> issues;

	@OneToMany(mappedBy = "contactPerson")
	@JsonIgnoreProperties("contactPerson")
	private List<IssueTask> issueTasks;

	@OneToMany(mappedBy = "contactPerson")
	@JsonIgnoreProperties("contactPerson")
	private List<Task> task;

	public ContactPerson()
	{
	}

	public ContactPerson( long contactId, String title, String firstName, String lastName,
			Designation designation )
	{
		this.contactId = contactId;
		this.title = title;
		this.firstName = firstName;
		LastName = lastName;
		this.designation = designation;
	}

	public ContactPerson( long contactId, String title, String firstName, String lastName, String email, String contactNo, Designation designation, String skypeID, List<Project> project, List<ProjectFunding> projectFundings, List<PreAwardTask> preAwardTasks, List<Issue> issues, List<IssueTask> issueTasks, List<Task> task )
	{
		this.contactId = contactId;
		this.title = title;
		this.firstName = firstName;
		LastName = lastName;
		this.email = email;
		this.contactNo = contactNo;
		this.designation = designation;
		this.skypeID = skypeID;
		this.project = project;
		this.projectFundings = projectFundings;
		this.preAwardTasks = preAwardTasks;
		this.issues = issues;
		this.issueTasks = issueTasks;
		this.task = task;
	}

	public long getContactId()
	{
		return contactId;
	}

	public void setContactId( long contactId )
	{
		this.contactId = contactId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle( String title )
	{
		this.title = title;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return LastName;
	}

	public void setLastName( String lastName )
	{
		LastName = lastName;
	}

	public Designation getDesignation()
	{
		return designation;
	}

	public void setDesignation( Designation designation )
	{
		this.designation = designation;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail( String email )
	{
		this.email = email;
	}

	public String getContactNo()
	{
		return contactNo;
	}

	public void setContactNo( String contactNo )
	{
		this.contactNo = contactNo;
	}

//	public List<Project> getProject()
//	{
//		return project;
//	}
//
//	public void setProject( List<Project> project )
//	{
//		this.project = project;
//	}

	public String getSkypeID()
	{
		return skypeID;
	}

	public void setSkypeID( String skypeID )
	{
		this.skypeID = skypeID;
	}

//	public List<ProjectFunding> getProjectFundings()
//	{
//		return projectFundings;
//	}
//
//	public void setProjectFundings( List<ProjectFunding> projectFundings )
//	{
//		this.projectFundings = projectFundings;
//	}

//	public List<PreAwardTask> getPreAwardTasks()
//	{
//		return preAwardTasks;
//	}
//
//	public void setPreAwardTasks( List<PreAwardTask> preAwardTasks )
//	{
//		this.preAwardTasks = preAwardTasks;
//	}

//	public List<Issue> getIssues()
//	{
//		return issues;
//	}
//
//	public void setIssues( List<Issue> issues )
//	{
//		this.issues = issues;
//	}

//	public List<IssueTask> getIssueTasks()
//	{
//		return issueTasks;
//	}
//
//	public void setIssueTasks( List<IssueTask> issueTasks )
//	{
//		this.issueTasks = issueTasks;
//	}
}
