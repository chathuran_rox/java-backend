package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.dao.ProjectFundingRepo;
import NationalProjectMonitoringPlatform.modle.ContactPerson;
import NationalProjectMonitoringPlatform.modle.FundingAgency;
import NationalProjectMonitoringPlatform.modle.ProjectFunding;
import NationalProjectMonitoringPlatform.service.AgencyService;
import NationalProjectMonitoringPlatform.service.ContactPersonService;
import NationalProjectMonitoringPlatform.service.ProjectFundingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProjectFundingController
{
	@Autowired
	private ProjectFundingService projectFundingService;

	@Autowired
	private ContactPersonService contactPersonService;

	@RequestMapping(value = "/api/saveFundings",method = RequestMethod.POST)
	public List<ProjectFunding> saveFundings(@RequestBody List<ProjectFunding> funding){
		return projectFundingService.savefundings( funding );
	}



}
