package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.modle.PreAwardTask;
import NationalProjectMonitoringPlatform.service.PreAwardTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PreAwardTaskController
{
	@Autowired
	public PreAwardTaskService preAwardTaskService;

	@RequestMapping(value = "/api/getAllPreAwardTasks",method = RequestMethod.GET)
	public List<PreAwardTask> getAllTasks(){return preAwardTaskService.getAllTasks();}

	@RequestMapping(value = "/api/savePreAwardTask",method = RequestMethod.POST)
	public PreAwardTask saveTask(@RequestBody PreAwardTask task){
		return preAwardTaskService.savePreAwardTask( task );}

	@RequestMapping(value = "/api/getChildTask/{id}", method = RequestMethod.GET)
	public List<PreAwardTask> getChildTasks(@PathVariable Long id){
		return preAwardTaskService.getChildTasks(id); }}