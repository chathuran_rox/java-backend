package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.modle.ExecutingAgency;
import NationalProjectMonitoringPlatform.modle.FundingAgency;
import NationalProjectMonitoringPlatform.modle.ImplementingAgency;
import NationalProjectMonitoringPlatform.service.AgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AgencyController
{
	@Autowired
	private AgencyService agencyService;

	@RequestMapping(value = "/api/getAllExeAgencies",method = RequestMethod.GET)
	public List<ExecutingAgency> getAllExeAgencies(){return agencyService.getAllExeAgencies();}

	@RequestMapping(value = "/api/getAllImpAgencies",method = RequestMethod.GET)
	public List<ImplementingAgency> getAllImpAgencies(){return agencyService.getAllImpAgencies();}

	@RequestMapping(value = "/api/getAllFundingAgencies",method = RequestMethod.GET)
	public List<FundingAgency> getAllFundingAgencies(){
		return agencyService.getAllFundingAgencies();
	}
}
