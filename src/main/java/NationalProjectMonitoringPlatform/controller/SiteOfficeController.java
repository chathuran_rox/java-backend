package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.dao.SiteOfficeRepo;
import NationalProjectMonitoringPlatform.modle.SiteOffice;
import NationalProjectMonitoringPlatform.modle.SiteOfficeSave;
import NationalProjectMonitoringPlatform.service.SiteOfficeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@Api(value="Site Office", description="Organization Structure Site Offices")
public class SiteOfficeController {
    @Autowired
    private SiteOfficeService siteOfficeService;

    @Autowired
    private SiteOfficeRepo siteOfficeRepo;

    @ApiOperation(value = "Insert a site office")
    @RequestMapping(value = "/api/saveSiteOffice",method = RequestMethod.POST, produces = "application/json")
    public SiteOfficeSave saveSiteOffice(@RequestBody @Valid  SiteOfficeSave siteOfficeSave){
        if( siteOfficeSave.getSiteOffice().getStaffMembers().size() > 0 )
        {
            siteOfficeSave.getSiteOffice().getStaffMembers().stream().forEach( staffMember -> {
                staffMember.setStaffMemberSiteOffice( siteOfficeSave.getSiteOffice() );
            } );
        }

        SiteOffice siteOfficeSaved= siteOfficeService.saveSiteOffice(siteOfficeSave.getSiteOffice(),  Long.valueOf(siteOfficeSave.getParentid()));

        SiteOfficeSave SiteOfficeSave2;
        if(siteOfficeSaved.getParent()!=null){
            SiteOfficeSave2 = new SiteOfficeSave(siteOfficeSaved, siteOfficeSaved.getParent().getSiteOfficeId());
        }
        else{
            SiteOfficeSave2 = new SiteOfficeSave(siteOfficeSaved, 0);
        }
        return SiteOfficeSave2;
    }
//
//    @ApiOperation(value = "Update a site office")
//    @RequestMapping(value = "/api/updateSiteOffice",method = RequestMethod.PUT, produces = "application/json")
//    public ResponseEntity<Object> updateSiteOffice(@RequestBody @Valid  SiteOfficeSave siteOfficeSave){
//        Optional<SiteOffice> siteOffice = Optional.ofNullable(siteOfficeService.getSiteOffice(siteOfficeSave.getSiteOfficeId()));
//        if (!siteOffice.isPresent())
//            return new ResponseEntity<>("{ \"message\" : \"SITE_OFFICE_NOT_FOUND\"}",
//                    HttpStatus.BAD_REQUEST);
//
//        if( siteOfficeSave.getSiteOffice().getStaffMembers().size() > 0 )
//        {
//            siteOfficeSave.getSiteOffice().getStaffMembers().stream().forEach( staffMember -> {
//                staffMember.setStaffMemberSiteOffice( siteOfficeSave.getSiteOffice() );
//            } );
//        }
//
//        siteOfficeSave.getSiteOffice().setSiteOfficeId(siteOfficeSave.getSiteOfficeId());
//
//        return ResponseEntity.ok().body(siteOfficeService.saveSiteOffice(siteOfficeSave.getSiteOffice(), Long.valueOf(siteOfficeSave.getParentid())));
//    }
//


    @ApiOperation(value = "Retrieve all Site Office Details with their parents")
    @RequestMapping(value = "/api/getAllSiteOffice",method = RequestMethod.GET, produces = "application/json")
    public List<SiteOfficeSave> getAll(){
        List<SiteOffice> list= siteOfficeService.getAll();

        List<SiteOfficeSave> li= new ArrayList<>(0);
        list.forEach(siteOffice -> {
            if(siteOffice.getParent()!=null){
                li.add(new SiteOfficeSave(siteOffice, siteOffice.getParent().getSiteOfficeId()));
            }
            else{
                li.add(new SiteOfficeSave(siteOffice, 0));
            }
        });

        li.forEach(siteOfficeSave -> {
            siteOfficeSave.getSiteOffice().setParent(null);
        });
        return li;
    }

    @ApiOperation(value = "Retrieve all Site Office Details with their parents By Project Id")
    @RequestMapping(value = "/api/getAllSiteOffice/{projectID}",method = RequestMethod.GET, produces = "application/json")
    public List<SiteOfficeSave> getAllSiteOfficeByProjectId(@PathVariable long projectID){
        List<SiteOffice> list= siteOfficeRepo.findByProjectID(projectID);

        List<SiteOfficeSave> li= new ArrayList<>(0);
        list.forEach(siteOffice -> {
            if(siteOffice.getParent()!=null){
                li.add(new SiteOfficeSave(siteOffice, siteOffice.getParent().getSiteOfficeId()));
            }
            else{
                li.add(new SiteOfficeSave(siteOffice, 0));
            }
        });

        li.forEach(siteOfficeSave -> {
            siteOfficeSave.getSiteOffice().setParent(null);
        });
        return li;
    }
}
