package NationalProjectMonitoringPlatform.controller;


import NationalProjectMonitoringPlatform.dao.ContactPersonsRepository;
import NationalProjectMonitoringPlatform.dao.DesignationsRepository;
import NationalProjectMonitoringPlatform.dao.InstitutesRepository;
import NationalProjectMonitoringPlatform.modle.ContactPersons;
import NationalProjectMonitoringPlatform.modle.Designations;
import NationalProjectMonitoringPlatform.modle.Institutes;
import io.swagger.annotations.Api;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value="Contact Persons", description="Contact Persons")
public class ContactPersonsController {

    @Autowired
    private DesignationsRepository designationsRepository;

    @Autowired
    private InstitutesRepository institutesRepository;

    @Autowired
    private ContactPersonsRepository contactPersonsRepository;


    @PostMapping(value = "/api/designation")
    private Designations insertDesignation(@RequestBody @Valid Designations designations){
        return designationsRepository.save(designations);
    }

    @PostMapping(value = "/api/institute")
    private Institutes insertInstitute(@RequestBody @Valid Institutes institutes){
        return institutesRepository.save(institutes);
    }


    @PostMapping(value = "/api/contactPerson")
    private ContactPersons insertContactPerson(@RequestBody @Valid ContactPersons contactPersons){
        return contactPersonsRepository.save(contactPersons);
    }


    @GetMapping(value = "/api/getAllDesignations")
    private List<Designations> getAllDesignations(){
        return designationsRepository.findAll();
    }

    @GetMapping(value = "/api/getAllInstitutes")
    private List<Institutes> getAllInstitutes(){
        return institutesRepository.findAll();
    }

    @GetMapping(value = "/api/getAllContactPersons")
    private List<ContactPersons> getAllContactPersons(){
        return contactPersonsRepository.findAll();
    }

    @GetMapping(value = "/api/getAllContactPersonsDetails")
    private AllContatDetails getAllContactPersonsDetails(){
        return new AllContatDetails(contactPersonsRepository.findAll(),institutesRepository.findAll(), designationsRepository.findAll());
    }

    @GetMapping(value = "/api/getAllInstitutesAndDesignations")
    private AllContatDetails getAllInstitutesAndDesignations(){
        return new AllContatDetails(null,institutesRepository.findAll(), designationsRepository.findAll());
    }
}


@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class AllContatDetails{
    private List<ContactPersons> contactPersons;
    private List<Institutes> institutes;
    private List<Designations> designations;

}
