package NationalProjectMonitoringPlatform.controller;


import NationalProjectMonitoringPlatform.modle.Comments;
import NationalProjectMonitoringPlatform.service.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CommentsController
{
	@Autowired
	private CommentsService commentsService;

	@RequestMapping(value = "/api/saveComments",method = RequestMethod.POST)
	public Comments saveAllComments(@RequestBody Comments comments){
		return commentsService.saveComments( comments );
	}

	@RequestMapping(value = "/api/getAllComments",method = RequestMethod.GET)
	public List<Comments> getAllComments(){
		return commentsService.getAll();
	}
}
