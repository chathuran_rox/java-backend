package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.dao.ProjectRepository;
import NationalProjectMonitoringPlatform.modle.PreAwardTask;
import NationalProjectMonitoringPlatform.modle.Project;
import NationalProjectMonitoringPlatform.modle.Task;
import NationalProjectMonitoringPlatform.service.ProjectService;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by chathuran on 8/20/2018.
 */
@RestController
@RequestMapping(value = "api/project")
public class ProjectController
{
	@Autowired
	private ProjectService projectService;

	@Autowired
	ProjectRepository pr;

	@RequestMapping(value = "/get_all",method = RequestMethod.GET)
	public List<Project> getProjects(){
		return  projectService.getAllProject();
	}

	@RequestMapping(value = "/test",method = RequestMethod.GET)
	public String test(){
		return "Test";
	}

	@RequestMapping(value = "/save",method = RequestMethod.POST)
	public Project saveProject(@RequestBody Project project){
		projectService.saveProject( project );
		return project;
	}

	@RequestMapping(value = "/getProject/{id}", method = RequestMethod.GET)
	public Project getProject(@PathVariable Long id)
	{
		return projectService.getProject( id );
	}

	@RequestMapping(value = "/getSingleProject/{id}", method = RequestMethod.GET)
	public Project getSingleProject(@PathVariable Long id)
	{
		return projectService.getSingleProject( id );
	}

//	@RequestMapping(value = "/save",method = RequestMethod.POST)
//	public Project saveProjectID(@RequestBody Project project){
//		projectService.saveProject( project );
//		return project;
//	}
	@RequestMapping(value = "/getProjectPreAward/{id}",method = RequestMethod.GET)
	public List<PreAwardTask> getProjectPreAward(@PathVariable Long id)
	{
		return  projectService.getAllPreAwards( id );
	}

	@RequestMapping(value = "/getProjectTaskBreakdown/{id}",method = RequestMethod.GET)
	public List<Task> getProjectTaskBreakdown(@PathVariable Long id)
	{
		return  projectService.getAllTaskBreakdown( id );
	}

	@GetMapping(value = "/getAllProjectsForProgressMonitoring")
	public List<Project> getAllProjectsForProgressMonitoring(){
		return projectService.getAllProjectsForProgressMonitoring();
	}

	@GetMapping(value = "/findByprojectNameLike/{page}/{size}/{projectName}")
	public Page<Project> findByprojectNameStartingWith(@PathVariable int  page, @PathVariable int size,@NotBlank @NotNull @PathVariable String projectName){
            return projectService.findByProjectNameStartingWith(projectName, new PageRequest(page, size , Sort.Direction.ASC, "projectName"));
	}

//	@GetMapping(value = "getAllIssues")
//	public List<Task>
	@GetMapping(value = "/select/{projectId}")
	public  List<PreAwardTask> getProject(@PathVariable long projectId){
		List<PreAwardTask> p= pr.findByProjectId(projectId).getPreAwardTaskSet();
		return p;
	}

	@GetMapping(value = "/getAllTasks/{projectId}")
	public  AllTasks getProjectTasks(@PathVariable long projectId){
//		return new AllTasks(projectService.getAllTaskBreakdown(projectId), projectService.getAllPreAwards(projectId));
		List<TaskPreAward> list= projectService.getAllPreAwards(projectId).stream().map
				(task -> new TaskPreAward(task.getId(), task.getTaskTitle()))
				.collect(Collectors.toList());
		List<TaskTask> list2= projectService.getAllTaskBreakdown(projectId).stream().map
				(task -> new TaskTask(task.getId(), task.getTaskTitle()))
				.collect(Collectors.toList());

		return  new AllTasks(list, list2);
	}

}


@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class AllTasks{
	private List<TaskPreAward> taskPreAwards;
	private List<TaskTask> taskTask;
}


@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class TaskPreAward{
	Long id;
	String preAwardTask;
}

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class TaskTask{
	Long id;
	String taskTask;
}


