package NationalProjectMonitoringPlatform.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.ws.Response;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FileUploadController
{
	private final Logger logger = LoggerFactory.getLogger( FileUploadController.class );
 	//Local Location
//	private static String UPLOAD_PATH ="D://temp1//";
	//Server Location
	private static String UPLOAD_PATH ="//usr//local//tomcat9//webapps//npmp-attachments//";

	//upload multiple files
	@PostMapping("api/upload/multi")
	public ResponseEntity<?> uploadMultipleFiles(@RequestParam("files") MultipartFile[] uploadfiles){

		if(uploadfiles.length ==0){
			return new ResponseEntity("please upload files", HttpStatus.OK);
		}

		String uploadFilelsName = Arrays.stream( uploadfiles ).map( x-> x.getOriginalFilename() ).filter( x-> !StringUtils.isEmpty( x ) ).collect( Collectors.joining(",") );

		try{
			saveUploadedFiles( Arrays.asList( uploadfiles ) );

		}catch ( Exception e ){
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);}

		return new ResponseEntity( "successfully uploaded file "+uploadFilelsName , HttpStatus.OK );
	}

	//save files
	public void saveUploadedFiles(List<MultipartFile> files) throws IOException
	{

		for(MultipartFile file : files){
			if(file.isEmpty()){continue;}
			byte[] bytes = file.getBytes();
			Path path = Paths.get( UPLOAD_PATH +file.getOriginalFilename() );
			Files.write( path,bytes );
		}

	}
}
