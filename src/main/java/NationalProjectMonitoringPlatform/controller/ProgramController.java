package NationalProjectMonitoringPlatform.controller;


import NationalProjectMonitoringPlatform.modle.Program;
import NationalProjectMonitoringPlatform.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProgramController
{
	@Autowired
	private ProgramService programService;

	@RequestMapping(value = "/api/saveProgram",method = RequestMethod.POST)
	public Program saveAllPrograms(@RequestBody Program program){
		return programService.saveProgram( program );
	}

	@RequestMapping(value = "/api/getProgramsNames",method = RequestMethod.GET)
	public List<String> getProgramTitles(){return programService.getProgramNames();}

	@RequestMapping(value = "/api/getFirstProgram/{id}",method = RequestMethod.GET)
	public Program getFirstPro(@PathVariable Long id){return programService.getFirstProgram( id );}

	@RequestMapping(value = "/api/getAllPRograms",method = RequestMethod.GET)
	public List<Program> getAllPrograms(){return programService.getAllPrograms();}
}
