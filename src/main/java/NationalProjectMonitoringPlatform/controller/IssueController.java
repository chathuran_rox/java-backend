package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.modle.Issue;
import NationalProjectMonitoringPlatform.modle.IssueTask;
import NationalProjectMonitoringPlatform.modle.Task;
import NationalProjectMonitoringPlatform.service.IssuesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class IssueController
{
	@Autowired
	private IssuesService issuesService;

	@RequestMapping(value ="/api/getAllIssues/{id}",method = RequestMethod.GET)
	public List<Issue> getAllIssues(@PathVariable Long id){
		return issuesService.getAllIssues(id);}

	@RequestMapping(value ="/api/getAllIssueTasks/{id}",method = RequestMethod.GET)
	public List<IssueTask> getAllIssueTasks(@PathVariable Long id){
		return issuesService.getAllIssueTasks( id );}

	@RequestMapping(value ="/api/saveIssue",method = RequestMethod.POST)
	public Issue saveIssue(@RequestBody Issue issue){
		return issuesService.saveIssue( issue ); }

	@RequestMapping(value = "/api/saveIssueTask",method = RequestMethod.POST)
	public IssueTask saveIssueTask(@RequestBody IssueTask issueTask){
		return  issuesService.saveIssueTask( issueTask );
	}


}
