package NationalProjectMonitoringPlatform.controller;


import NationalProjectMonitoringPlatform.dao.IssueAssignRepository;
import NationalProjectMonitoringPlatform.dao.IssueRepository;
import NationalProjectMonitoringPlatform.modle.IssueAssign;
import NationalProjectMonitoringPlatform.modle.IssueModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IssueAssignController {

    @Autowired
    private IssueAssignRepository issueAssignRepository;

    @Autowired
    private IssueRepository issueRepository;

    @PostMapping(value = "/api/issueAssign")
    public IssueAssign saveIssueAssign(@RequestBody IssueAssign issueAssign){
        IssueModel issueModel= issueRepository.getOne(issueAssign.getIssueID());
        issueModel.setType("ASSIGNED");
        issueRepository.save(issueModel);
        return issueAssignRepository.save(issueAssign);
    }
}
