package NationalProjectMonitoringPlatform.controller;


import NationalProjectMonitoringPlatform.dao.TaskRepo;
import NationalProjectMonitoringPlatform.modle.Task;
import NationalProjectMonitoringPlatform.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskController
{
	@Autowired
	private TaskService taskService;

	@Autowired
	private TaskRepo taskRepo;

	@RequestMapping(value = "/api/saveTask",method = RequestMethod.POST)
	public Task saveTask(@RequestBody  Task task){
		return taskService.saveTask( task );
	}

	@RequestMapping(value = "/api/getTasks",method = RequestMethod.GET)
	public List<Task> getTasks(){
		return  taskService.getTask();
	}

	@RequestMapping(value = "/api/updateLeaf/{id}",method = RequestMethod.POST)
	public Task updateLeaf(@PathVariable Long id){
		return taskService.updateLeaf( id );}

	@RequestMapping(value = "/api/getChildren/{id}",method = RequestMethod.GET)
	public List<Task> getChildren(@PathVariable Long id){
		return taskService.getChildren( id );
	}

	@RequestMapping(value = "/api/progress/physical/progressUpdate",method = RequestMethod.POST)
	public Task updateTaskProgressAndStatus(@RequestBody Task task){
		return taskService.updateTaskProgressAndStatus( task );
	}

//	@GetMapping(value = "/api/tasksList")
//	public List<Task> getAllTasks(){
//		return taskRepo.getAllTasks();
//	}
}
