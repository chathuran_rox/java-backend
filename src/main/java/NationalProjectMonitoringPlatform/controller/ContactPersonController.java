package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.modle.ContactPerson;
import NationalProjectMonitoringPlatform.service.ContactPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ContactPersonController
{
	@Autowired
	private ContactPersonService contactPersonService;

	@RequestMapping(value = "/api/getAllContact",method = RequestMethod.GET)
	public List<ContactPerson> getAll(){
		return contactPersonService.getAll();
	}
}
