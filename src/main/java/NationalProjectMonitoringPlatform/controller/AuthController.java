package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.dao.RoleRepository;
import NationalProjectMonitoringPlatform.dao.UserRepository;
import NationalProjectMonitoringPlatform.mail.EmailServiceImpl;
import NationalProjectMonitoringPlatform.message.request.LoginForm;
import NationalProjectMonitoringPlatform.message.request.SignUpForm;

import NationalProjectMonitoringPlatform.modle.Role;
import NationalProjectMonitoringPlatform.modle.RoleName;
import NationalProjectMonitoringPlatform.modle.User;
import NationalProjectMonitoringPlatform.message.response.JwtResponse;
import NationalProjectMonitoringPlatform.security.jwt.JwtProvider;
import lombok.*;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    EmailServiceImpl emailService;


    PasswordGenerator generator = new PasswordGenerator();



    List rules = Arrays.asList(new CharacterRule(EnglishCharacterData.UpperCase, 2),
            new CharacterRule(EnglishCharacterData.LowerCase, 6), new CharacterRule(EnglishCharacterData.Digit, 7));

    @PostMapping(value = "/signin", produces = "application/json")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {
        if(!userRepository.existsByUsername(loginRequest.getUsername())) {
            return new ResponseEntity<>("{ \"errorMessage\" : \"INVALID_USERNAME\"}",
                    HttpStatus.BAD_REQUEST);
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        User user=userRepository.findByUsername(loginRequest.getUsername()).orElse(null);
        String jwt = jwtProvider.generateJwtToken(authentication);
        loginResponse loginResponse= new loginResponse(getUser(user.getId()), new JwtResponse(jwt));
        return ResponseEntity.ok(loginResponse);
    }

    @GetMapping(value = "/jwtExpireCheck")
    public boolean checkJWTExpired(HttpServletRequest request){
        System.out.println("jwt token" + request.getHeader("Authorization").substring(7));
        return jwtProvider.validateJwtToken(request.getHeader("Authorization").substring(7) );
    }

    @PostMapping(value = "/signup", produces = "application/json")
    @PreAuthorize("hasRole('ROOT')")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>("{ \"errorMessage\" : \"USERNAME_EXIST\"}",
                    HttpStatus.BAD_REQUEST);
        }

        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>("{ \"errorMessage\" : \"EMAIL_EXIST\"}",
                    HttpStatus.BAD_REQUEST);
        }

        String password = generator.generatePassword(15, rules);



        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), encoder.encode(password));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        strRoles.forEach(role -> {
            switch(role) {
                case "ROLE_ROOT":
                    Role ROLE_ROOT = roleRepository.findByName(RoleName.ROLE_ROOT)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_ROOT);

                    break;
                case "ROLE_ADMIN":
                    Role ROLE_ADMIN = roleRepository.findByName(RoleName.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_ADMIN);

                    break;

                case "ROLE_SECRETARY":
                    Role ROLE_SECRETARY = roleRepository.findByName(RoleName.ROLE_SECRETARY)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_SECRETARY);

                    break;
                case "ROLE_PROJECT_DIRECTOR":
                    Role ROLE_PROJECT_DIRECTOR = roleRepository.findByName(RoleName.ROLE_PROJECT_DIRECTOR)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_PROJECT_DIRECTOR);

                    break;

                case "ROLE_PROJECT_MANAGER":
                    Role ROLE_PROJECT_MANAGER = roleRepository.findByName(RoleName.ROLE_PROJECT_MANAGER)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_PROJECT_MANAGER);

                    break;
                case "ROLE_CONTRACTOR":
                    Role ROLE_CONTRACTOR = roleRepository.findByName(RoleName.ROLE_CONTRACTOR)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_CONTRACTOR);

                    break;

                case "ROLE_ISSUE_MANAGING_PERSON":
                    Role ROLE_ISSUE_MANAGING_PERSON = roleRepository.findByName(RoleName.ROLE_ISSUE_MANAGING_PERSON)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_ISSUE_MANAGING_PERSON);

                    break;
                case "ROLE_PROJECT_UNIT_DIRECTOR":
                    Role ROLE_PROJECT_UNIT_DIRECTOR = roleRepository.findByName(RoleName.ROLE_PROJECT_UNIT_DIRECTOR)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_PROJECT_UNIT_DIRECTOR);

                    break;


                case "ROLE_CABINET":
                    Role ROLE_CABINET = roleRepository.findByName(RoleName.ROLE_CABINET)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_CABINET);

                    break;
                case "ROLE_PROCUMENT_COMMITEE":
                    Role ROLE_PROCUMENT_COMMITEE = roleRepository.findByName(RoleName.ROLE_PROCUMENT_COMMITEE)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_PROCUMENT_COMMITEE);

                    break;

                case "ROLE_ACCOUNTANT":
                    Role ROLE_ACCOUNTANT = roleRepository.findByName(RoleName.ROLE_ACCOUNTANT)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_ACCOUNTANT);

                    break;
                case "ROLE_USER":
                    Role ROLE_USER = roleRepository.findByName(RoleName.ROLE_USER)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
                    roles.add(ROLE_USER);

                default:

            }
        });

        user.setRoles(roles);
        user.setReset(false);
        userRepository.save(user);

        emailService.RegisterUser(user.getName(), password, user.getEmail());

        return ResponseEntity.ok().body("{ \"message\" : \"SUCCESSFULLY_ADDED\"}");
    }

    @PostMapping(value = "/forgotPassword/{username}", produces = "application/json")
    public ResponseEntity<String> forgetPassword(@PathVariable @Email String username) {
        if(!userRepository.existsByUsername(username)) {
            return new ResponseEntity<String>("{ \"message\" : \"INVALID_USERNAME\"}",
                    HttpStatus.BAD_REQUEST);
        }

        String password = generator.generatePassword(15, rules);

        User user= userRepository.findByUsername(username).orElse(null);

        if(user == null){
            return ResponseEntity.ok().body("Error");
        }else{
            user.setPassword(encoder.encode(password));
            user.setReset(false);
            userRepository.save(user);
            emailService.sendSimpleMessage("npmp.lka@gmail.com", "New Password", "Tour Password has been resetted successfully. Your new password is :" + password);

            return ResponseEntity.ok().body("{ \"message\" : \"PASSWORD_REQUEST_SENT_SUCCESSFULLY\"}");
        }
    }

    @PostMapping(value = "/changePassword")
    @PreAuthorize("hasRole('ROOT')")
    public ResponseEntity<?> changePassword(@Valid @RequestBody LoginForm changePassword){
        if(!userRepository.existsByUsername(changePassword.getUsername())) {
            return new ResponseEntity<>("{ \"message\" : \"INVALID_USERNAME\"}",
                    HttpStatus.BAD_REQUEST);
        }
        User user= userRepository.findByUsername(changePassword.getUsername()).orElse(null);


        if(user == null){
            return new ResponseEntity<>("{ \"message\" : \"USER_NOT_FOUND\"}",
                    HttpStatus.BAD_REQUEST);
        }else{
            user.setPassword(encoder.encode(changePassword.getPassword()));
            user.setReset(true);
            userRepository.save(user);
            emailService.sendSimpleMessage("npmp.lka@gmail.com", "New Password", "Your Password Have Been Changed" );

            return ResponseEntity.ok().body("{ \"message\" : \"PASSWORD_CHANGED_SUCCESSFULLY\"}");
        }
    }

    @PostMapping(value = "/html")
    public boolean html(){
        try{
            emailService.htmlemail();
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public User getUser(long userId){
        return  userRepository.getOne(userId);
    }

    @GetMapping(value = "/user/{username}")
    public User getUser(@PathVariable @Email String username){
        return  userRepository.findByUsername(username).orElse(null);
    }
}

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class loginResponse{
    private User user;
    private JwtResponse jwtResponse;
}