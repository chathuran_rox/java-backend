package NationalProjectMonitoringPlatform.controller;

import NationalProjectMonitoringPlatform.dao.IssueRepository;
import NationalProjectMonitoringPlatform.mail.EmailServiceImpl;
import NationalProjectMonitoringPlatform.modle.IssueModel;
import NationalProjectMonitoringPlatform.modle.Uploadedfiles;
import NationalProjectMonitoringPlatform.payload.UploadFileResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;



@RestController
public class IssueCtrl {

    @Autowired
    private FileController fileController;

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    EmailServiceImpl emailService;

    @PostMapping(value = "/api/createIssue")
    public IssueModel saveIssue(@RequestParam("file") MultipartFile[] files, @RequestParam("issue") String issueasJson) throws Exception{
        IssueModel issue  = new ObjectMapper().readValue(issueasJson, IssueModel.class);
        List<UploadFileResponse> uploadedFile;
        try {
            uploadedFile = fileController.uploadMultipleFiles(files, "ISSUE");
        } catch (Exception ex){
            return new IssueModel();
        }

        List<Uploadedfiles> uploadedFiles = new ArrayList<>( 0 );
        if(uploadedFile.size()> 0){
            for (int i = 0; i < uploadedFile.size(); i++) {
                uploadedFiles.add(new Uploadedfiles(uploadedFile.get(i).getFileName(),
                        uploadedFile.get(i).getFileDownloadUri(), uploadedFile.get(i).getFileType(),
                        uploadedFile.get(i).getSize(), "ISSUE"));
            }
        }
        issue.setUploadedFiles(uploadedFiles);

        issue.setType("CREATED");

        issue.getUploadedFiles().stream().forEach(uploadedfiles ->  uploadedfiles.setIssue(issue));

        emailService.sendSimpleMessage("dcedannoruwa@gmail.com", "Issue Created", "Your Issue has beed created successfully. Thanks you");
        return issueRepository.save(issue);
    }

    @GetMapping(value = "/api/getAllIssues")
    public List<IssueModel> getAllIssurs(){
        return issueRepository.findAll();
    }

    @GetMapping(value = "/api/getAllCreatedIssues")
    public List<IssueModel> getAllCreatedIssurs(){
        return issueRepository.findByType("CREATED");
    }

    @GetMapping(value = "/api/getAllAssignedIssues")
    public List<IssueModel> getAllAssignedIssurs(){
        return issueRepository.findByType("ASSIGNED");
    }

}
