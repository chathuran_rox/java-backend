package NationalProjectMonitoringPlatform;

import NationalProjectMonitoringPlatform.Properties.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class NationalProjectMonitoringPlatformApplication extends SpringBootServletInitializer
{
	public static void main(String[] args) {
		SpringApplication.run(NationalProjectMonitoringPlatformApplication.class, args);
	}
}
