package NationalProjectMonitoringPlatform;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket siteOfficeApi() {
        return new Docket(DocumentationType.SWAGGER_2).
                select().
                apis(RequestHandlerSelectors.basePackage("NationalProjectMonitoringPlatform")).
                paths(regex("/api.*")).
                build().
                apiInfo(metaData());
    }

    private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
                "NPMP",
                "National Project Management Platform",
                "1.0",
                "Terms of service",
                new Contact("Npmp Develepment Team", "https://npmp/about/", ""),
                "License Version 2.0",
                "https://npmp/licenses/LICENSE-2.0");

        return apiInfo;

    }


}